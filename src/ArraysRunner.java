import java.util.Random;

/**
 * Created by administrator on 8/3/18.
 */
public class ArraysRunner {
    public static void main(String[] args) {

        //int[] intArray = fillRandom(); //передаем результат работы метода переменной массива

        int[] intArray = fillRandom();
        int min = intArray[0];
        int max = intArray[0];
        int indexMin = 0;
        int indexMax = 0;

        for (int i = 0; i < intArray.length; i++) {
            System.out.print(intArray[i] + " ");
        }
        System.out.println();
        System.out.print("here min is: " + intArray[findMin(intArray)] + " with index " + findMin(intArray));
        System.out.println();
        System.out.println("and max is: " + intArray[findMax(intArray)] + " wih index " + findMax(intArray));
        System.out.println();
        //System.out.println(" catch left part: " +
         printLeftHalf(intArray);
        System.out.println();
         printRightHalf(intArray);
        System.out.println();
        invertArray(intArray);

    }

    public static int[] fillRandom() {
        int[] intArray = new int[10];
        Random random = new Random(); //creating random values>0
        for (int i = 0; i < intArray.length; i++) {
            if (i % 2 == 0) {
                int random1 = (int) (Math.random() * 100) * -1; //creating random values <0
                intArray[i] = random1;
            } else {
                intArray[i] = random.nextInt(90); //adding bound for random values(not>90)
            }

        }
        return intArray;
    }

    public static int findMax(int [] intArray) {
        int max = intArray[0];
        int indexMax = 0;
        for (int i = 0; i < intArray.length; i++) {
            if (max < intArray[i]) {
                max = intArray[i];
                indexMax = i;
            }
        }
        return indexMax;
    }

    public static int findMin(int [] intArray){
        int min = intArray[0];
        int indexMin=0;
        for (int i = 0; i < intArray.length; i++) {
            if (min > intArray[i]) { //searching for a minimum value by comparing min with each array element;
                min = intArray[i];
                indexMin = i;
            }
        }
        return indexMin;
    }
    public static void printLeftHalf(int[] intArray){  //method printing the left part of array
        for (int i = 0; i < intArray.length/2 ; i++) {
            System.out.print(intArray[i] +" ");
        }
    }
    public static void printRightHalf(int[] intArray){  //method printing the right half of array
        for (int i = intArray.length/2;  i < intArray.length ; i++) {
            System.out.print(intArray[i] +" ");
        }
    }

    public static void invertArray(int[] intArray){
        int indexTemp =0;
        int indexOne=0;
        int indexTwo=0;
        for (int i = 0; i < intArray.length; i++) {
        indexTemp = intArray.length-(intArray.length-indexOne);
        indexOne=indexTemp;
        indexTwo=indexTemp;
            System.out.print(intArray[indexTemp] +" ");
        }

    }
}


