package diceware;

import java.util.Random;

/**
 * Created by administrator on 8/24/18.
 */
public class DiceThrower {

    private static final int ROLLS = 4;

    public static final int SIDES = 6;

    public static int throwDice() {

        Random random = new Random();

        int result = 0;

        for (int i = 0; i < ROLLS; i++) {

            int num= random.nextInt(SIDES)+1;

            result += num*(Math.pow(10,i));

        }


        return result;

    }

}
