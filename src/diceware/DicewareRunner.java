package diceware;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.StringJoiner;

/**
 * Created by administrator on 8/18/18.
 */
public class DicewareRunner {

    public static final String PASS_FILE = "src/main/resources/pass";

    public static void main(String[] args) {
   /*     ArrayList list = new ArrayList();
        list.add(1);
        list.add(3);
        list.add(8);
        list.add(" ");
        list.add('u');

        Integer integer =new Integer(1); //=Integer.ValueOf(1) // wrapper class where object=digit
        for (Object o : list){

      */ // System.out.println(o);
    /*     }

       ArrayList<Integer> integers = new ArrayList<>();
        integers.add(5); //here only digits can be added
        integers.add(5);
        integers.add(5);
        integers.add(5);
        integers.add(5);

     */  // System.out.println(integers + " " + " list size is: "  + integers.size());


        /*
        Map<Integer,String> dictionary = new HashMap<>();

        dictionary.put(6565, "wipe"); //- unique key - if added several times- still 1 record
        dictionary.put(6565, "wiped"); //we can change

        System.out.println("dictionary size: " + dictionary.size());
        String s = dictionary.get(6565);
        System.out.println(s);
        */

        //System.out.println(DiceThrower.throwDice());


        int dice = DiceThrower.throwDice();
        System.out.println(dice);


        WordlistReader reader = new WordlistReader();
        Map<Integer, String> dictionary = reader.readWordList();

        String word = dictionary.get(dice);
        System.out.println(word);

        int complexity =5;
        String password = getPassword(dictionary, complexity);
        System.out.println(password);
        saveToFile(password);

    }

    public static String getPassword(Map<Integer, String> dictionary, int NumOfWords){

        StringJoiner joiner= new StringJoiner(" ", "","");
        for (int i = 0; i < NumOfWords; i++) {

            int key = DiceThrower.throwDice();
            String value = dictionary.get(key);
            joiner.add(value);
        }
        return joiner.toString();
    }

    public static void saveToFile(String password) {

        Path path= Paths.get(PASS_FILE);
        try {
       //     Files.deleteIfExists(path);
       //
            boolean exists= Files.exists(path);
                    if (!exists) {
                        Files.createFile(path);
                    }
            Files.write(path, password.concat("\n").getBytes(),
                    StandardOpenOption.APPEND); // saves all passwords in the same file one after another

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
