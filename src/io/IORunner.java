package io;

import java.io.File;
import java.io.IOException;
import java.time.ZoneId;
import java.util.Scanner;

public class IORunner {
    public static final  String RES ="src/ttext/";
    public static  void main (String[] args){
        String path = RES ;
       //TextFileReader.readFileByteByByte(path);
        TextFileReader.readFileLineByLine(path);


       /* File file = new File(RES + "out");
        boolean exists = file.exists();
        if (!exists){ //exists -shows is file exists by given path
            try {
                boolean created = file.createNewFile();
                if (created) System.out.println("File created");
            } catch (IOException e){
                e.printStackTrace();
            }
        }
        TextFileReader.readAndWriteFile(path, file.getPath());
*/
        crawl(new File("src"), ".*\\.png");
    }

    private  static void crawl (File file, String suffix){ ///recursive

       if (file.exists()){
           File[] files = file.listFiles(File::isFile);
           if(files!=null){
               for (File f : files){
                  //here we can add logic f.e if (f.getName().matches(suffix)) and add this suffix to where method is called
                   if (f.getName().matches(suffix))
                   System.out.println(f); //here we can call fileDelete to remove particular files described in logic above
               }
           }
           File [] dirs = file.listFiles(File::isDirectory); //esli eto directioria - zasovivaem v massiv
           if (dirs !=null){
               for (File dir : dirs){
                   crawl(dir,suffix);
               }
           }
       }
    }
}


