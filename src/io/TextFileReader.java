package io;

import java.io.*;

public class TextFileReader {
    public static final String RES = "src/ttext/";

    public static void readFileByteByByte(String path) {


      /*  Побитовое чтение
        InputStream in = null;
        try {
            in = new FileInputStream(path);
            int read;
            while ((read = in.read()) != -1) {
                System.out.println((char) read);
            }

        } catch (FileNotFoundException e) {
            System.out.println("File is not found " + e.getMessage());
            ;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeSilently(in);
        }
    }

    private static void closeSilently(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    */

        //потоковый ридер

    }

    public static void readFileLineByLine(String path) {
        BufferedReader reader = null;

        try{
            reader = new BufferedReader(new FileReader(path));
            boolean end =false;
            while (!end){

            String line = reader.readLine();
            if (line != null){
              if (line.matches("hello.*")) //show lines with 'hello%'
                  System.out.println(line);
            } else end =true;
            }


        } catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            closeSilently(reader);
        }

    }

    public static void readAndWriteFile(String source, String target) {  //читатель, читающий построчно + писатель, пишущий построчно
        BufferedReader in = null;
        BufferedWriter out= null;

        try {
            in = new BufferedReader(new FileReader(source));
            out= new BufferedWriter(new FileWriter(target));

            boolean end = false;
            while (!end) {
                String line = in.readLine(); //читаем каждую строчку сорса
                if (line != null){
                    out.write(line);  //если строка not null - write line
                    out.newLine(); //для перехода на след.строку
                } else end =true;
            }

        } catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            closeSilently(in);
            closeSilently(out);
        }


    }
    private static void closeSilently(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}