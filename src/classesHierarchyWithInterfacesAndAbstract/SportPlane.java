package classesHierarchyWithInterfacesAndAbstract;

public class SportPlane extends Airplane {
    @Override
    public void fly() {
        super.fly();
    }

    @Override
    public void land() {
        super.land();
    }
}
