package classesHierarchyWithInterfacesAndAbstract;

public interface Flyable {
    void fly();
    void land();
}
