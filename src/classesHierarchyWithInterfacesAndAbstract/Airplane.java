package classesHierarchyWithInterfacesAndAbstract;

public class Airplane extends AbstractMachine implements Flyable {
    @Override
    public void fly() {

    }

    @Override
    public void land() {

    }

    @Override
    public void startEngine() {

    }

    @Override
    public void stopEngine() {

    }
}
