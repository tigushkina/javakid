package classesHierarchyWithInterfacesAndAbstract;

public interface Swimmable {
    void swim();
}
