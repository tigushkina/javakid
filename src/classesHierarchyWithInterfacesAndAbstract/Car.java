package classesHierarchyWithInterfacesAndAbstract;

public class Car extends AbstractMachine implements Drivable{

    @Override
    public void startEngine() {

    }

    @Override
    public void stopEngine() {

    }

    @Override
    public void drive() {

    }

    @Override
    public void stop() {

    }
}
