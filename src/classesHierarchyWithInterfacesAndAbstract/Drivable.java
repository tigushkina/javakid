package classesHierarchyWithInterfacesAndAbstract;

public interface Drivable {
    void drive();
    void stop();
}
