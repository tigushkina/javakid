package classesHierarchyWithInterfacesAndAbstract;

public class TransportPlane extends Airplane {
    @Override
    public void fly() {
        super.fly();
    }

    @Override
    public void land() {
        super.land();
    }
}
