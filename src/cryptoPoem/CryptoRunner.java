package cryptoPoem;

import java.io.*;

/**
 * Created by administrator on 8/17/18.
 */
public class CryptoRunner {

    public static void main(String[] args) {

        File encrypted = new File("src/main/resources/poem_crypt");

        PoemCrypter.encrypt(encrypted);
        System.out.println((int) 'a');

    }
}


class PoemCrypter {

    private static final String CRYPT = "_crypt";

    private static final String DECRYPT = "_decrypt";

    public static void encrypt(File file) { //передаеим незашифрованый файл, чтоб не удалить наш оригинальнвый файл

        File candidate = createCandidate(file, CRYPT);
        Reader reader = null;
        Writer writer = null;

        try {

            reader = new FileReader(file);
            writer = new FileWriter(candidate);

            boolean working = true;
            while (working) {

                int letter = reader.read();//возвращает каждый символ поочереди
                if (letter != -1) {
                    int shifted = shiftArithmetic(letter); //здвигаем по уравнению из шифтАрифметик
                    writer.write(shifted);
                } else working = false; //чтоб не заходить в вайл когда мы достигнем -1 (несуществ.код в юникоде)
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeSilently(reader);
            closeSilently(writer);
        }
    }
    private static File createCandidate(File file, String suffix) { //creaing  file for our decrypting text

        File candidate = new File(file.getPath() + suffix);
        if (candidate.exists()) {
            candidate.delete(); //if it exists- we delete old one to put there a new file
        }
        try {
            candidate.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return candidate;
    }

    public static int shiftArithmetic(int value) {
        //There is something missing...
       return (value - 31) / 17;
        //return 31+17*value;


    }



    private static void closeSilently(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
