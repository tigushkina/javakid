import java.util.Scanner;

/**
 * Created by administrator on 7/22/18.
 */
public class ScannerThreeCompare {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Hi! type 3 values for A, B, and C one by one: ");

        int A = scan.nextInt();
        int B =scan.nextInt();
        int C =scan.nextInt();

        if (A>B && A>C){
            System.out.println("A is the biggest");
        }
        else if (B>A && B>C){
            System.out.println("B is the biggest");
        }
        else if (C>A && C>B){
            System.out.println("C is biggest");
        }

        if (A<B && A<C){
            System.out.println("A is the smallest");
        }
        else if (B<A && B<C){
            System.out.println("B is the smallest");
        }
        else if (C<A && C<B){
            System.out.println("C is the smallest");
        }


    }
}
