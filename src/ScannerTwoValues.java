import java.util.Scanner;

/**
 * Created by administrator on 7/19/18.
 */
public class ScannerTwoValues {
    public static void main(String[] args) {

        //Создать программу, которая попросит у пользователя ввести в консоль число A, а потом число B. После этого программа должна вывести в консоль следующую информацию:
       // - какое из двух чисел больше - A или B. Если числа равны, то вывести об этом сообщение в консоль.
        // - делится ли большее число на меньшее без остатка.

        Scanner scan = new Scanner(System.in);

        System.out.println("pls type two numbers: ");

        int value = scan.nextInt();
        int valueTwo = scan.nextInt();

        int max =0;
        int min =0;

        if(value==valueTwo){
            System.out.println("values are equal");
        }
        else if (value>valueTwo){
            System.out.println("first value is bigger");
            max=value;
            min=valueTwo;
        }
        else {
            System.out.println("second value is bigger");
            min=value;
            max=valueTwo;
        }

        //int maximum = Math.max(value, valueTwo);
        //int min= Math.min(value, valueTwo);

      if (max%min == 0){
           System.out.println("you can divide values without remainder");
        }
        else{
           System.out.println("can be divided - but with the remainder :( ");
       }



    }

}
